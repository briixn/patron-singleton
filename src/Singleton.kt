object Singleton {
    var contador= 0

    fun aumentar(num:Int) {
        contador+= num
    }

}

//Singleton es usado cuando se encuentran varios objetos que realizan un mismo trabajo o poseen la misma informacion
//en diferentes actividades. Es decir, cuando se instancia y crea muchas veces objetos de una misma claase.
//la solucion es instanciar el objeto una sola vez de manera global y estatica.